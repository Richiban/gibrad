﻿namespace GibRAD.Tests.Helpers

open System

type TestObject(item1:String, item2:String) =

    member this.Item1 = item1
    member this.Item2 = item2