using System;

namespace GibRAD
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Method for safely parsing an int. The default value is returned if the parse failed.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="default"></param>
        /// <returns></returns>
        public static int ParseIntOrDefault( this Object data, int @default = 0 )
        {
            int result;

            var success = int.TryParse( data.ToString(), out result );

            return success ? result : @default;
        }

        public static TOut SmartCast<TIn, TOut>(this Object item, Func<TOut> castFails, Func<TIn, TOut> castSucceeds)
        {
            return item is TIn ? castSucceeds((TIn) item) : castFails();
        }
    }
}