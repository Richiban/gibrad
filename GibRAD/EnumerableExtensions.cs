﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GibRAD
{
    public static class EnumerableExtensions
    {
        /// <summary>
        ///     Pages the given collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber">Page number is a 1-based index!</param>
        /// <returns></returns>
        public static IEnumerable<T> Page<T>(this IEnumerable<T> source, int pageSize, int pageNumber)
        {
            int toSkip = pageSize * Math.Max(pageNumber - 1, 0);

            var result = source.Skip(toSkip).Take(pageSize);

            return result;
        }
    }
}