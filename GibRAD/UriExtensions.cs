﻿using System;
using System.Web;

namespace GibRAD
{
    public static class UriExtensions
    {
        public static string GetQueryItem( this Uri @this, string key )
        {
            var queryCollection = HttpUtility.ParseQueryString( @this.Query );

            return queryCollection[key];
        }
    }
}
