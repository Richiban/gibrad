using System;
using System.Xml.Linq;

namespace GibRAD
{
    public static class XElementExtensions
    {
        public static String ValueOrDefault(this XElement @this, string defaultValue = null)
        {
            return @this == null ? defaultValue : @this.Value;
        }
    }
}