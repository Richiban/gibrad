﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GibRAD
{
    public static class StringExtensions
	{
		/// <summary>
		///	Takes some HTML (with paragraphs in it), and returns the first
		/// two paragraphs of the original text
		/// </summary>
		/// <param name="original"></param>
		/// <param name="paraLimit"></param>
		/// <returns></returns>
		public static String ParagraphChop( this String original, int paraLimit = 2 )
		{
			try
			{
				var found = new List<int>();
				for ( int i = 0; i < original.Length; i++ )
				{
					if ( found.Count > paraLimit )
						break;

					if ( original.Substring( i, 4 ) == "</p>" )
						found.Add( i + 4 );
				}
				return original.Substring( 0, found[paraLimit - 1] ) + "<p>[...]</p>";
			}
			catch ( Exception )
			{
				return original;
			}
		}

		/// <summary>
		/// Extension for String.Format
		/// </summary>
		/// <param name="formatString"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static String F( this String formatString, params Object[] args )
		{
			return String.Format( formatString, args );
		}

        public static DateTime? ParseDateOrNull( this string @this )
        {
            DateTime result;
            return DateTime.TryParse(@this, out result) ? (DateTime?) result : null;
        }

        public static string SafeJoin(this string separator, params object[] items)
        {
            var itemsWithValue = items.Where(ItemIsNotEmptyString);

            return string.Join(separator, itemsWithValue);
        }

        private static bool ItemIsNotEmptyString(object item)
        {
            return item.SmartCast(
                () => true, 
                (string s) => !string.IsNullOrWhiteSpace(s) );
        }
	}
}