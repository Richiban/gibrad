﻿using System;

namespace GibRAD
{
    public static class DateTimeExtensions
    {
        public static int YearsSince(this DateTime to, DateTime from)
        {
            to = to.Date;
            from = from.Date;

            var yearDiff = to.Year - from.Year;

            return from.AddYears(yearDiff) <= to
                ? yearDiff
                : yearDiff - 1;
        }
    }
}
